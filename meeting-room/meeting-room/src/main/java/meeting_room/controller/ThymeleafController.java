package meeting_room.controller;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import meeting_room.dto.MeetingCreateDto;
import meeting_room.service.MeetingService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("meeting")
@RequiredArgsConstructor
@Tag(name = "MeetingController", description = "Meeting Controller API ")
public class ThymeleafController {
    private final MeetingService meetingService;
    @GetMapping
    public String getMeeting(Model model,
                             @RequestBody(required = false)MeetingCreateDto request) {
        model.addAttribute("request", request);
//        model.addAttribute("meetingService", meetingService.addMeeting(request));
        return "meeting-form";
    }
}
