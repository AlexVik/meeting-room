package meeting_room.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class PhoneExistsException extends RuntimeException {
    private static final String PHONE_EXISTS = "Пользователь с таким телефоном существует!";
    public PhoneExistsException(){super(PHONE_EXISTS);}
}
